
module ChefGraphite
  # Helps generate ini format configuration files for Carbon
  module Config
    # Takes a Enumerable and spits out a ini file.
    # Hash should contain hashes named for their ini section.
    # Section hashes contain items to be configured and their values.
    #
    #  ```
    #  foo = {
    #    garbage_collection: {
    #      pattern: 'garbageCollections$',
    #      retentions: '10s:14d'
    #    }
    #  }
    #  ```
    #
    # Produces
    #
    # ```
    # [garbage_collection]
    # pattern = garbageCollections$
    # retentions = 10s:14d
    # ```
    #
    # @param config type [Enumerable]
    # @return 'mold' the formatted string.
    def self.hash_to_ini(config)
      mold = ''
      config.each do |section, settings|
        mold << "\n[#{ section }]\n"
        settings.each do |key, value|
          mold << "#{ key } = #{ value }\n"
        end
      end
      mold
    end

    # Takes an Enumerable and normalizes Mashes and Chef things
    # to standard Hashes and Arrays
    #
    # @param config type [Enumerable]
    # @return config with Mashes, etc replaced with ruby primitives
    def self.normalize(config)
      if config.is_a?(Chef::Node::ImmutableMash) or config.is_a?(Mash)
        config = config.to_hash
        config.inject({}) do |h,(k,v)|
          h[k] = normalize(v)
          h
        end
      elsif config.is_a?(Chef::Node::ImmutableArray)
        config = config.to_a
        config.map { |x| normalize(x) }
      else
        config
      end
    end

    # Takes an Enumerable and spits out a YAML file.
    #
    # @param config type [Enumerable]
    # @return 'mold' the serialized YAML
    def self.hash_to_yaml(config)
      require 'yaml'
      YAML::dump(normalize(config.dup))
    end
  end
end
