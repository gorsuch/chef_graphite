
file node[:graphite][:api][:config_path] do
  owner node[:graphite][:user]
  group node[:graphite][:group]
  mode 00644
  content lazy {
    ChefGraphite::Config.hash_to_yaml(node[:graphite][:api][:config])
  }
end
