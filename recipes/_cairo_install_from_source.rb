cairo_source = "#{ node[:graphite][:cairo][:source][:url] }" \
               "/py2cairo-#{ node[:graphite][:cairo][:source][:version] }" \
               '.tar.gz'

python_pip cairo_source do
  virtualenv node[:graphite][:virtualenv]
  action :install
end
